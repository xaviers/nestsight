import { NestFactory } from '@nestjs/core';
import { AppModule } from '@app/app.module';
import { custom } from 'openid-client';
import { APP_CONFIG } from '@config/app.config';

async function bootstrap() {
  custom.setHttpOptionsDefaults({
    retry: APP_CONFIG.openidClientHttpRetry,
  });

  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(APP_CONFIG.appPort);
}
bootstrap();
