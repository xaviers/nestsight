import { Controller, Get } from '@nestjs/common';
import { DiversityReportService } from '@modules/reports/services/diversity-report.service';
import { PublicReportType } from '@modules/reports/models/report-types.model';

@Controller('diversity')
export class DiversityReportController {
  constructor(
    private readonly diversityReportService: DiversityReportService,
  ) {}

  @Get()
  getDiversityPublicReport(): Promise<PublicReportType | undefined> {
    return this.diversityReportService.getPublicReport();
  }
}
