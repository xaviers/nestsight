import { Controller, Get } from '@nestjs/common';
import { PerformanceReportService } from '@app/modules/reports/services/performance-report.service';
import { PublicReportType } from '@modules/reports/models/report-types.model';

@Controller('performance')
export class PerformanceReportController {
  constructor(
    private readonly performanceReportService: PerformanceReportService,
  ) {}

  @Get()
  getPerformanceReport(): Promise<PublicReportType | undefined> {
    return this.performanceReportService.getPublicReport();
  }
}
