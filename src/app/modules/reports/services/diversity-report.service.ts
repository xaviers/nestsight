import { Injectable } from '@nestjs/common';
import { DiversityReport } from '@modules/reports/models/diversity-report/diversity-report.model';
import { RequestService } from '@shared/services/request.service';
import { ReportService } from '@modules/reports/models/report-service.model';
import { API_ENDPOINTS } from '@shared/constants/api-endpoints.constant';
import { SHARESIGHT_CONFIG } from '@config/sharesight.config';
import { lastValueFrom, map } from 'rxjs';

@Injectable()
export class DiversityReportService extends ReportService {
  constructor(private readonly requestService: RequestService) {
    super();
  }

  async fetchReport(): Promise<void> {
    const report$ = this.requestService
      .get<DiversityReport>(
        API_ENDPOINTS.DIVERSITY_REPORT(SHARESIGHT_CONFIG.portfolioId),
      )
      .pipe(
        map((response) => {
          return new DiversityReport(response.data);
        }),
      );

    this.report = await lastValueFrom(report$);
  }
}
