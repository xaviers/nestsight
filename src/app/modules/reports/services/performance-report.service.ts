import { Injectable } from '@nestjs/common';
import { PerformanceReport } from '@app/modules/reports/models/performance-report/performance-report.model';
import { lastValueFrom, map } from 'rxjs';
import { API_ENDPOINTS } from '@shared/constants/api-endpoints.constant';
import { SHARESIGHT_CONFIG } from '@config/sharesight.config';
import { RequestService } from '@shared/services/request.service';
import { ReportService } from '@modules/reports/models/report-service.model';
import { AxiosRequestConfig } from 'axios';

@Injectable()
export class PerformanceReportService extends ReportService {
  constructor(private readonly requestService: RequestService) {
    super();
  }

  async fetchReport(): Promise<void> {
    const config: AxiosRequestConfig = {
      params: {
        include_sales: true,
      },
    };

    const report$ = this.requestService
      .get<PerformanceReport>(
        API_ENDPOINTS.PERFORMANCE_REPORT(SHARESIGHT_CONFIG.portfolioId),
        config,
      )
      .pipe(
        map((response) => {
          return new PerformanceReport(response.data);
        }),
      );

    this.report = await lastValueFrom(report$);
  }
}
