import { Module } from '@nestjs/common';
import { PerformanceReportService } from '@modules/reports/services/performance-report.service';
import { PerformanceReportController } from '@modules/reports/controllers/performance-report.controller';
import { SharedModule } from '@shared/shared.module';
import { DiversityReportController } from '@modules/reports/controllers/diversity-report.controller';
import { DiversityReportService } from '@modules/reports/services/diversity-report.service';

const controllers = [DiversityReportController, PerformanceReportController];
const services = [DiversityReportService, PerformanceReportService];

@Module({
  imports: [SharedModule],
  providers: [...services],
  controllers: [...controllers],
})
export class ReportsModule {}
