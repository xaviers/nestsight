import { DiversityPublicReport } from '@modules/reports/models/diversity-report/diversity-public-report.model';
import { PerformancePublicReport } from '@modules/reports/models/performance-report/performance-public-report.model';
import * as dayjs from 'dayjs';
import { APP_CONFIG } from '@config/app.config';

export abstract class Report {
  readonly links: {
    readonly portfolio: string;
    readonly self: string;
  };
  readonly timestamp: number;
  readonly expiresAt: number;

  protected constructor(json: Record<string, any>) {
    this.links = {
      portfolio: json.portfolio,
      self: json.self,
    };

    const now = dayjs();
    this.timestamp = now.valueOf();
    this.expiresAt = now
      .add(APP_CONFIG.cacheDuration, APP_CONFIG.cacheDurationUnit)
      .valueOf();
  }

  isOutdated(): boolean {
    return this.expiresAt < dayjs().valueOf();
  }

  abstract toPublic(): PerformancePublicReport | DiversityPublicReport;
}
