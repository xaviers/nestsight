import {
  PublicReportType,
  ReportType,
} from '@modules/reports/models/report-types.model';

export abstract class ReportService {
  protected report: ReportType | undefined;

  async getPublicReport(): Promise<PublicReportType | undefined> {
    if (!this.report || this.report.isOutdated()) {
      await this.fetchReport();
    }

    return this.report?.toPublic();
  }

  protected abstract fetchReport(): Promise<void>;
}
