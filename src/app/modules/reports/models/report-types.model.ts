import { DiversityReport } from '@modules/reports/models/diversity-report/diversity-report.model';
import { PerformanceReport } from '@modules/reports/models/performance-report/performance-report.model';
import { DiversityPublicReport } from '@modules/reports/models/diversity-report/diversity-public-report.model';
import { PerformancePublicReport } from '@modules/reports/models/performance-report/performance-public-report.model';

export type ReportType = DiversityReport | PerformanceReport;
export type PublicReportType = DiversityPublicReport | PerformancePublicReport;
