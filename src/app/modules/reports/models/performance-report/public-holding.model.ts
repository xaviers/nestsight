export interface PublicHolding {
  readonly id: number;
  readonly symbol: string;
  readonly market: string;
  readonly name: string;
  readonly active: boolean;
  readonly capitalGainPercent: number;
  readonly payoutGainPercent: number;
  readonly currencyGainPercent: number;
  readonly totalGainPercent: number;
}
