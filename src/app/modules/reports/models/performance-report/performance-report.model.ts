import { Holding } from '@app/modules/reports/models/performance-report/holding.model';
import { PerformancePublicReport } from '@modules/reports/models/performance-report/performance-public-report.model';
import { Report } from '@modules/reports/models/report.model';

export class PerformanceReport extends Report {
  readonly id: string;
  readonly portfolioId: number;
  readonly grouping: string;
  readonly customGroupId: number;
  readonly value: number;
  readonly capitalGain: number;
  readonly capitalGainPercent: number;
  readonly payoutGain: number;
  readonly payoutGainPercent: number;
  readonly currencyGain: number;
  readonly currencyGainPercent: number;
  readonly totalGain: number;
  readonly totalGainPercent: number;
  readonly startDate: Date;
  readonly endDate: Date;
  readonly includeSales: boolean;
  readonly holdings: Holding[];

  constructor(json: Record<string, any>) {
    super(json);
    this.id = json.id;
    this.portfolioId = json.number;
    this.grouping = json.grouping;
    this.customGroupId = json.custom_group_id;
    this.value = json.value;
    this.capitalGain = json.capital_gain;
    this.capitalGainPercent = json.capital_gain_percent;
    this.payoutGain = json.payout_gain;
    this.payoutGainPercent = json.payout_gain_percent;
    this.currencyGain = json.currency_gain;
    this.currencyGainPercent = json.currency_gain_percent;
    this.totalGain = json.total_gain;
    this.totalGainPercent = json.total_gain_percent;
    this.startDate = new Date(json.start_date);
    this.endDate = new Date(json.end_date);
    this.includeSales = json.include_sales;
    this.holdings = json.holdings.map(
      (holdingJson: Record<string, any>) => new Holding(holdingJson),
    );
  }

  toPublic(): PerformancePublicReport {
    return {
      id: this.id,
      capitalGainPercent: this.capitalGainPercent,
      payoutGainPercent: this.payoutGainPercent,
      currencyGainPercent: this.currencyGainPercent,
      totalGainPercent: this.totalGainPercent,
      startDate: this.startDate,
      endDate: this.endDate,
      includeSales: this.includeSales,
      holdings: this.holdings.map((holding) => holding.toPublicHolding()),
      timestamp: this.timestamp,
    };
  }
}
