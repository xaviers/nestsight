import { PublicHolding } from '@modules/reports/models/performance-report/public-holding.model';

export interface PerformancePublicReport {
  readonly id: string;
  readonly capitalGainPercent: number;
  readonly payoutGainPercent: number;
  readonly currencyGainPercent: number;
  readonly totalGainPercent: number;
  readonly startDate: Date;
  readonly endDate: Date;
  readonly includeSales: boolean;
  readonly holdings: PublicHolding[];
  readonly timestamp: number;
}
