import { PublicHolding } from '@modules/reports/models/performance-report/public-holding.model';

export class Holding {
  readonly id: number;
  readonly symbol: string;
  readonly instrumentId: number;
  readonly market: string;
  readonly grouping: string;
  readonly name: string;
  readonly value: number;
  readonly quantity: number;
  readonly capitalGain: number;
  readonly capitalGainPercent: number;
  readonly payoutGain: number;
  readonly payoutGainPercent: number;
  readonly currencyGain: number;
  readonly currencyGainPercent: number;
  readonly totalGain: number;
  readonly totalGainPercent: number;

  constructor(json: Record<string, any>) {
    this.id = json.id;
    this.symbol = json.symbol;
    this.instrumentId = json.instrument_id;
    this.market = json.market;
    this.grouping = json.grouping;
    this.name = json.name;
    this.value = json.value;
    this.quantity = json.quantity;
    this.capitalGain = json.capital_gain;
    this.capitalGainPercent = json.capital_gain_percent;
    this.payoutGain = json.payout_gain;
    this.payoutGainPercent = json.payout_gain_percent;
    this.currencyGain = json.currencyGain;
    this.currencyGainPercent = json.currency_gain_percent;
    this.totalGain = json.totalGain;
    this.totalGainPercent = json.total_gain_percent;
  }

  toPublicHolding(): PublicHolding {
    return {
      id: this.id,
      symbol: this.symbol,
      market: this.market,
      name: this.name,
      active: this.quantity > 0,
      capitalGainPercent: this.capitalGainPercent,
      payoutGainPercent: this.payoutGainPercent,
      currencyGainPercent: this.currencyGainPercent,
      totalGainPercent: this.totalGainPercent,
    };
  }
}
