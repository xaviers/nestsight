import { DiversityPublicGroup } from '@modules/reports/models/diversity-report/diversity-public-group.model';

export interface DiversityPublicReport {
  readonly groups: Record<string, DiversityPublicGroup>;
  readonly percentage: number;
  readonly date: Date;
  readonly timestamp: number;
}
