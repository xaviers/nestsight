import { DiversityGroup } from '@app/modules/reports/models/diversity-report/diversity-group.model';
import { DiversityPublicReport } from '@modules/reports/models/diversity-report/diversity-public-report.model';
import { DiversityPublicGroup } from '@modules/reports/models/diversity-report/diversity-public-group.model';
import { Report } from '@modules/reports/models/report.model';

export class DiversityReport extends Report {
  readonly groups: Record<string, DiversityGroup>;
  readonly percentage: number;
  readonly value: number;
  readonly date: Date;

  constructor(json: Record<string, any>) {
    super(json);
    const groups: Record<string, DiversityGroup> = {};

    json.groups.forEach((groupJson: Record<string, any>) => {
      Object.keys(groupJson).forEach((groupName) => {
        groups[groupName] = new DiversityGroup(groupJson[groupName]);
      });
    });

    this.groups = groups;
    this.percentage = json.percentage;
    this.value = json.value;
    this.date = new Date(json.date);
  }

  toPublic(): DiversityPublicReport {
    const publicGroups: Record<string, DiversityPublicGroup> = {};

    Object.keys(this.groups).forEach((key) => {
      const group = this.groups[key] as DiversityGroup;
      publicGroups[key] = group.toPublic();
    });

    return {
      groups: publicGroups,
      percentage: this.percentage,
      date: this.date,
      timestamp: this.timestamp,
    };
  }
}
