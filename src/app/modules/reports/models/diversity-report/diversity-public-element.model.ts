export interface DiversityPublicElement {
  readonly name: string;
  readonly code: string;
  readonly market: string;
  readonly percentage: number;
}
