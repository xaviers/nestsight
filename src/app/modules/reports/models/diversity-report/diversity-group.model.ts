import { DiversityElement } from '@app/modules/reports/models/diversity-report/diversity-element.model';
import { DiversityPublicGroup } from '@modules/reports/models/diversity-report/diversity-public-group.model';

export class DiversityGroup {
  readonly elements: DiversityElement[];
  readonly percentage: number;
  readonly value: number;

  constructor(json: Record<string, any>) {
    this.elements = json.elements.map(
      (element: Record<string, any>) => new DiversityElement(element),
    );
    this.percentage = json.percentage;
    this.value = json.value;
  }

  toPublic(): DiversityPublicGroup {
    return {
      elements: this.elements.map((element) => element.toPublic()),
      percentage: this.percentage,
    };
  }
}
