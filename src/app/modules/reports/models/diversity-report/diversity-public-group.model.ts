import { DiversityPublicElement } from '@modules/reports/models/diversity-report/diversity-public-element.model';

export interface DiversityPublicGroup {
  readonly elements: DiversityPublicElement[];
  readonly percentage: number;
}
