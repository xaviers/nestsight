import { DiversityPublicElement } from '@modules/reports/models/diversity-report/diversity-public-element.model';

export class DiversityElement {
  readonly name: string;
  readonly code: string;
  readonly market: string;
  readonly percentage: number;
  readonly value: number;

  constructor(json: Record<string, any>) {
    this.name = json.name;
    this.code = json.code;
    this.market = json.market;
    this.percentage = json.percentage;
    this.value = json.value;
  }

  toPublic(): DiversityPublicElement {
    return {
      name: this.name,
      code: this.code,
      market: this.market,
      percentage: this.percentage,
    };
  }
}
