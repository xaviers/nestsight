import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { from, Observable, switchMap } from 'rxjs';
import { AuthTokenService } from '@shared/services/auth-token.service';

@Injectable()
export class TokenRefresherInterceptor implements NestInterceptor {
  constructor(private readonly sharesightService: AuthTokenService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    if (this.sharesightService.isTokenExpired()) {
      return from(this.sharesightService.updateAccessToken()).pipe(
        switchMap(() => next.handle()),
      );
    }

    return next.handle();
  }
}
