import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { TokenRefresherInterceptor } from '@shared/interceptors/token-refresher.interceptor';
import { AuthTokenService } from '@shared/services/auth-token.service';
import { RequestService } from '@shared/services/request.service';

const modules = [HttpModule];
const services = [AuthTokenService, RequestService];
const interceptors = [
  {
    provide: APP_INTERCEPTOR,
    useClass: TokenRefresherInterceptor,
  },
];

@Module({
  imports: [...modules],
  exports: [...modules, ...services],
  providers: [...interceptors, ...services],
})
export class SharedModule {}
