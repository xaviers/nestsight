export const API_URL = 'https://api.sharesight.com';
const API_ROOT = `${API_URL}/api/v2`;
const PORTFOLIO_ROOT = `${API_ROOT}/portfolios`;

export const API_ENDPOINTS = {
  DIVERSITY_REPORT: (portfolioId: string) =>
    `${PORTFOLIO_ROOT}/${portfolioId}/diversity.json`,
  PERFORMANCE_REPORT: (portfolioId: string) =>
    `${PORTFOLIO_ROOT}/${portfolioId}/performance.json`,
};
