import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, Issuer, TokenSet } from 'openid-client';
import { SHARESIGHT_CONFIG } from '@config/sharesight.config';
import { API_URL } from '@shared/constants/api-endpoints.constant';

@Injectable()
export class AuthTokenService implements OnModuleInit {
  private authClient: Client;
  private tokenSet: TokenSet;

  async onModuleInit(): Promise<void> {
    const issuer = new Issuer({
      issuer: API_URL,
      authorization_endpoint: SHARESIGHT_CONFIG.authorizeTokenUri,
      token_endpoint: SHARESIGHT_CONFIG.accessTokenUri,
    });

    this.authClient = new issuer.Client({
      client_id: SHARESIGHT_CONFIG.clientId,
      client_secret: SHARESIGHT_CONFIG.clientSecret,
    });

    await this.updateAccessToken();
  }

  isTokenExpired(): boolean {
    return this.tokenSet.expired();
  }

  getAccessToken(): string {
    if (!this.tokenSet.access_token) {
      throw new Error('Access token is undefined');
    }

    return this.tokenSet.access_token;
  }

  async updateAccessToken(): Promise<void> {
    try {
      this.tokenSet = await this.authClient.grant({
        grant_type: 'client_credentials',
      });
    } catch (err) {
      console.error('Failed to retrieve access token');
      throw err;
    }
  }
}
