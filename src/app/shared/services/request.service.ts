import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AuthTokenService } from '@shared/services/auth-token.service';
import { Observable } from 'rxjs';
import { AxiosRequestConfig, AxiosResponse } from 'axios';

@Injectable()
export class RequestService {
  constructor(
    private readonly httpService: HttpService,
    private readonly authTokenService: AuthTokenService,
  ) {}

  get<T>(
    url: string,
    config?: AxiosRequestConfig,
  ): Observable<AxiosResponse<T>> {
    const updatedConfig = {
      ...this.defaultConfig(),
      ...config,
    };

    return this.httpService.get(url, updatedConfig);
  }

  private defaultConfig(): AxiosRequestConfig {
    return {
      headers: {
        Authorization: `Bearer ${this.authTokenService.getAccessToken()}`,
      },
    };
  }
}
