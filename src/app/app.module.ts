import { Module } from '@nestjs/common';
import { ReportsModule } from '@modules/reports/reports.module';
import { RouterModule } from '@nestjs/core';

@Module({
  imports: [
    ReportsModule,
    RouterModule.register([
      {
        path: 'reports',
        module: ReportsModule,
      },
    ]),
  ],
  providers: [],
})
export class AppModule {}
