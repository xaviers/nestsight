export const SHARESIGHT_CONFIG = {
  clientId: 'YOUR_CLIENT_ID',
  clientSecret: 'YOUR_CLIENT_SECRET',
  authorizeTokenUri: 'YOUR_AUTHORIZE_TOKEN_URI',
  accessTokenUri: 'YOUR_ACCESS_TOKEN_URI',
  portfolioId: 'YOUR_PORTFOLIO_ID',
};
