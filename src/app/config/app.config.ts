import { OpUnitType } from 'dayjs';

const APP_PORT = 3000;

const CACHE_DURATION = 2;
const CACHE_DURATION_UNIT: OpUnitType = 'days';

const OPENID_CLIENT_RETRY_LIMIT = 3;

export const APP_CONFIG = {
  appPort: APP_PORT,
  cacheDuration: CACHE_DURATION,
  cacheDurationUnit: CACHE_DURATION_UNIT,
  openidClientHttpRetry: OPENID_CLIENT_RETRY_LIMIT,
};
